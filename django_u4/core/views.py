# -*- coding: utf-8 -*-

from datetime import datetime
from dateutil.parser import parse
from django.http import HttpResponse
from django.shortcuts import render
from django.views.generic import ListView
from django_u4.core.models import Foo

# Create your views here.
class FooListView(ListView):
    model = Foo
    template_name = 'foo_list.html'

class FooRangeListView(ListView):
    model = Foo
    template_name = 'foo_list.html'

    def get(self, request, date1, date2):
        #zakladamy, ze sa
        #date1 = request.GET['date1']
        #date2 = request.GET['date2']

        try:
            date1 = datetime.strptime(date1, '%Y/%m/%d').strftime('%Y-%m-%d')
            date2 = datetime.strptime(date2, '%Y/%m/%d').strftime('%Y-%m-%d')
        except ValueError:
            return HttpResponse(u'Zła data!')

        object_list = Foo.objects.filter(date__gte=date1, date__lte=date2)
        return render(request, self.template_name, {'object_list': object_list})

class FooWeightListView(ListView):
    model = Foo
    template_name = 'foo_list.html'

    def get(self, request):
        try:
            weight = request.GET.get('weight', 200)
            date = datetime.strptime(request.GET.get('date', '2013/12/15'), '%Y/%m/%d').strftime('%Y-%m-%d')
        except ValueError:
            return HttpResponse(u'Złe dane!')
        object_list = Foo.objects.filter(weight__lt=weight, date=date)
        return render(request, self.template_name, {'object_list': object_list})
