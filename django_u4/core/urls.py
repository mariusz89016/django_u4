from django.conf.urls import patterns, include, url
from django_u4.core.views import FooListView, FooRangeListView, FooWeightListView

urlpatterns = patterns('',
        url(r'^list/$', FooListView.as_view()),
        url(r'^listweight/$', FooWeightListView.as_view()),
        url(r'^list/(?P<date1>\d{4}/\d{2}\/\d{2})-(?P<date2>\d{4}/\d{2}/\d{2})', FooRangeListView.as_view()),
)